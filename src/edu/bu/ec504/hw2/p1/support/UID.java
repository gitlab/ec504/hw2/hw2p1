package edu.bu.ec504.hw2.p1.support;

/**
 * Represents objects with a unique ID.
 * No different objects of this class should have the same ID.
 * Objects of the same Base class should have successively incrementing IDs.
 */
public class UID<Base> {
    public UID() {
        myID = currID++;
    }
    final public int myID;

    /**
     * The current ID, incremented with each new object of type Base.
     */
    private static int currID = 0;
}
