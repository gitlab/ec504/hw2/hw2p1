package edu.bu.ec504.hw2.p1.graphs;

import edu.bu.ec504.hw2.p1.edges.Edge;
import edu.bu.ec504.hw2.p1.vertices.Vertex;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Represents an undirected, unweighted graph.
 * @author Ari Trachtenberg
 */

public abstract class Graph<VertexClass extends Vertex, EdgeClass extends Edge<VertexClass>> {

    // SUBCLASSES

    // METHODS

    // ... QUERY
    /**
     * @return the number of vertices currently in the graph.
     */
    public int numVertices() {
        return vertices.size();
    }

    /**
     * @param ii An integer between 0 [inclusive] and numVertices() [exclusive]
     * @return The ii-th vertex that was inserted into the graph.
     */
    public VertexClass ithVertex(int ii) { return vertices.get(ii); }

    /**
     * @return true iff there is an edge connecting exactly the vertices in Vlist
     */
    abstract public boolean adjQ(ArrayList<VertexClass> Vlist);

    /**
     * An alternate form of {@link #adjQ(ArrayList)}.
     */
    @SafeVarargs
    public final boolean adjQ(VertexClass... V) {
        List<VertexClass> ls = Arrays.asList(V);
        return adjQ(new ArrayList<>(ls));
    }

    /**
     * @return A human-readable version of the graph
     */
    public abstract String toString();

    // ... MANIPULATION

    /**
     * Adds a new vertex to the graph (not part of any edges).
     * @param theVertex the new vertex to add
     * @return theVertex
     */
    public abstract VertexClass addVertex(VertexClass theVertex);

    /**
     * Adds an edge to the graph
     * @param theEdge the edge to add
     */
    public abstract void addEdge(EdgeClass theEdge);


    // DATA FIELDS

    /** The vertices of the graph. */
    protected final ArrayList<VertexClass> vertices = new ArrayList<>();
}
